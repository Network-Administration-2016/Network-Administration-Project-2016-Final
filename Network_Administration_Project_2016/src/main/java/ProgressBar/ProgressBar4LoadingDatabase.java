package ProgressBar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;

import readers.CSVReaderIPScanner;
import readers.CSVReaderNetView;
import readers.CSVReaderWifiInfoView;
import readers.CSVReaderWireShark;
import readers.CSVReaderXirrus;
import javax.swing.UIManager;
import java.awt.Font;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class ProgressBar4LoadingDatabase {

	private int selection = 0;
	private JFrame frame;
	private JTextArea textArea;
	private File fileToOpen;

	/**
	 * Create the application.
	 * @param fileToOpen 
	 */
	public ProgressBar4LoadingDatabase(int selection, File fileToOpen) {
		this.selection = selection;
		this.fileToOpen = fileToOpen;
		initialize();
	}
	
	/**
	 * 
	 * @author netarios
	 * @author netarios
	 *
	 */
	private class Task_UpdateDatabase extends SwingWorker<Void, Void> {

        JProgressBar progressBar;
        
        /**
         * 
         * @param progressBar
         */
        public Task_UpdateDatabase(JProgressBar progressBar) {
           this.progressBar = progressBar;                                                                                                                                                                                                                                                                                                                                                                                             
        }
		

		@Override
		protected Void doInBackground() {
			
			if(fileToOpen == null || selection == 0){
				return null;				
			}
			
			System.out.println(selection);
			
			switch (selection){
			case 1:
		    	CSVReaderXirrus xirrus = new CSVReaderXirrus(fileToOpen.getAbsolutePath(), progressBar, textArea);
		    	xirrus.upload_CSVRederXirrus_info();
				break;
			case 2:
		    	CSVReaderWireShark wireshark = new CSVReaderWireShark(fileToOpen.getAbsolutePath(), progressBar, textArea);  
		    	wireshark.upload_CSVReaderWireShark_info();
				break;
			case 3:
		    	CSVReaderIPScanner ipscanner = new CSVReaderIPScanner(fileToOpen.getAbsolutePath(), progressBar, textArea);
		    	ipscanner.upload_DBIPScanner_info();
				break;
			case 4:
		    	CSVReaderNetView netview = new CSVReaderNetView(fileToOpen.getAbsolutePath(), progressBar, textArea); 	
		    	netview.upload_CSVReaderNetView_info();
				break;
			case 5:
		    	CSVReaderWifiInfoView wifiinfoview = new CSVReaderWifiInfoView(fileToOpen.getAbsolutePath(), progressBar, textArea);			    	
		    	wifiinfoview.upload_CSVReaderWifiInfoView_info();
				break;
				
			}
			return null;
		}
		
        @Override
        protected void done() {
            try {
                try {
					get(1, TimeUnit.MILLISECONDS);
				} catch (TimeoutException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                JOptionPane.showMessageDialog(this.progressBar.getParent(), "Completed", "Completed", JOptionPane.INFORMATION_MESSAGE);
                frame.dispose();
			} catch (CancellationException e) {
                e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				cancel(true); 
			}
        }
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
        frame.setTitle("Reading Files and Updating Database");
        frame.setAlwaysOnTop(false);
        frame.setResizable(false);
        
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	
		    	int option = JOptionPane.showConfirmDialog(frame, "Warning: Continouing Will Terminate Programm!","Exit", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
		    	
		    	if (option == JOptionPane.YES_OPTION){
		    		System.out.println("exited");
		    		frame.dispose();
		    		System.exit(0);
		    	}else{
		    		System.out.println("not exited");
		    	}
		    	
		    }
		});
		
		
		//http://stackoverflow.com/questions/20260372/swingworker-progressbar#20270356
		
		//http://stackoverflow.com/questions/23167319/how-to-use-java-progress-bar-while-read-a-text-file
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(25, 107, 398, 53);
		frame.getContentPane().add(progressBar);
		
		
		JLabel lblNewLabel = new JLabel("Please Wait......");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(25, 27, 398, 33);
		frame.getContentPane().add(lblNewLabel);
		
		textArea = new JTextArea();
		textArea.setFont(new Font("Dialog", Font.BOLD, 12));
		textArea.setEditable(false);
		textArea.setBackground(UIManager.getColor("Button.background"));
		textArea.setBounds(177, 72, 102, 25);
		frame.getContentPane().add(textArea);
		
		final Task_UpdateDatabase myWorker = new Task_UpdateDatabase(progressBar);
		myWorker.execute();
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
							
		    	int option = JOptionPane.showConfirmDialog(frame, "Warning: Continouing Will Terminate Programm!","Exit", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
		    	if (option == JOptionPane.YES_OPTION){
		    		System.out.println("exited");
		    		frame.dispose();
		    		System.exit(0);
		    	}
				myWorker.cancel(true);
				frame.dispose();
			}
		});
		btnCancel.setBounds(162, 205, 117, 25);
		frame.getContentPane().add(btnCancel);
		
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
