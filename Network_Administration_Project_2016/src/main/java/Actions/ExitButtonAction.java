package Actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class ExitButtonAction implements ActionListener {
	
	private JFrame frame;
	
	public ExitButtonAction(JFrame frame){
		this.frame = frame;
	}

	/**
	 * action when hitting exit button or closing the window
	 * a confirm dialog pops up
	 */
	public void actionPerformed(ActionEvent arg0) {
    	int option = JOptionPane.showConfirmDialog(null, "Are you sure to exit?","Exit", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
		//int option = JOptionPane.showConfirmDialog(null, "Are you sure to exit?","Exit", JOptionPane.INFORMATION_MESSAGE);
    	if (option == JOptionPane.YES_OPTION){
    		System.out.println("exited");
    		frame.dispose();
    		System.exit(0);
    	}else{
    		System.out.println("not exited");
    	}

	}

}
