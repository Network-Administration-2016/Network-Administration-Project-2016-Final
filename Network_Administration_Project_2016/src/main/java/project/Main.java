package project;

import java.awt.EventQueue;

import GUI.MainGUI;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;

import database.DBCredentials_and_ConnectionPool;
import database.DBLogin;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class Main 
{
	
	// Database driver and URL
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost:3306/mydb?useSSL=false";
			
	//Database credentials
	private static final String USER = "root";
	private static final String PASSWORD = "root";
	
	private static DBCredentials_and_ConnectionPool db_credentials_and_connnection_pool;
	
	public static DBCredentials_and_ConnectionPool getDb_credentials() {
		return db_credentials_and_connnection_pool;
	}

	private static DBLogin dbLogin;
	
	public static DBLogin getDb_users() {
		return dbLogin;
	} 
	
	//http://www.jolbox.com
	private static BoneCPConfig config = new BoneCPConfig();
	private static BoneCP connectionPool = null;
	
	public static BoneCP getConnectionPool() {
		return connectionPool;
	} 
	
    public static void main( String[] args ) throws ClassNotFoundException
    {
    	//Class.forName(JDBC_DRIVER);
    	    	
 
		try {
			// load the database driver (make sure this is in your classpath!)
			Class.forName(JDBC_DRIVER);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		try {

			//http://www.jolbox.com/
			// setup the connection pool
			config.setJdbcUrl(DB_URL); // jdbc url specific to your database, eg jdbc:mysql://127.0.0.1/yourdb
			config.setUsername(USER); 
			config.setPassword(PASSWORD);
			config.setMinConnectionsPerPartition(5);
			config.setMaxConnectionsPerPartition(10);
			config.setPartitionCount(1);
			connectionPool = new BoneCP(config); // setup the connection pool
			
		} catch (Exception e) {
			e.printStackTrace();
			return;	
		}
    	
		db_credentials_and_connnection_pool = new DBCredentials_and_ConnectionPool(DB_URL,USER,PASSWORD,connectionPool);
  	    	
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI window = new MainGUI();
					window.getFrame().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

    }
}
