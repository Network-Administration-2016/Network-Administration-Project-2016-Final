package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.DefaultTableModel;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */
public class DBIPScanner extends Database {

	/**
	 * 
	 * @param cred
	 */
	public DBIPScanner(DBCredentials_and_ConnectionPool cred) {
		super(cred);
	}
	
	/**
	 * 
	 * @param State
	 * @param Name
	 * @param IP
	 * @param Radmin
	 * @param Http
	 * @param Https
	 * @param Ftp
	 * @param Rdp
	 * @param Common_Dirs
	 * @param Team
	 * @param Manufacturer
	 * @param MAC_Address
	 * @return 0 on success
	 */
	public int insert_DBIPScanner_stats (String State,String Name,String IP,String Radmin,
			String Http,String Https,String Ftp,
			String Rdp,String Common_Dirs, String Team,
			String Manufacturer,String MAC_Address){
		
		Statement stmt = null;
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		

		try {	
			
			//http://stackoverflow.com/questions/5005388/cannot-add-or-update-a-child-row-a-foreign-key-constraint-fails
			stmt.executeQuery("SET FOREIGN_KEY_CHECKS=0");
			
			
			stmt.executeUpdate("INSERT INTO IPScanner(State,Name,IP,Radmin,Http,Https,Ftp,Rdp,Common_Dirs,Team,Manufacturer,MAC_Address)"
					+ " VALUES('" 
					+ State
					+ "','"
					+ Name
					+ "','"
					+ IP
					+ "','"
					+ Radmin
					+ "','"
					+ Http
					+ "','"
					+ Https
					+ "','"
					+ Ftp 
					+ "','"
					+ Rdp 
					+ "','"
					+ Common_Dirs 
					+ "','"
					+ Team 
					+ "','"
					+ Manufacturer 
					+ "','"
					+ MAC_Address + "');");	
			
			stmt.executeQuery("SET FOREIGN_KEY_CHECKS=1");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	/**
	 * 
	 * @return DefaultTableModel model to set to JTable
	 * @throws SQLException
	 */
	public DefaultTableModel selectIPScannerData() throws SQLException{
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			results = stmt.executeQuery("SELECT `IPScanner`.`State`, `IPScanner`.`Name`,"
							+ "`IPScanner`.`IP`,`IPScanner`.`Radmin`, "
							+ "`IPScanner`.`Http`,`IPScanner`.`Https`,`IPScanner`.`Ftp`,`IPScanner`.`Rdp`, "
							+ "`IPScanner`.`Common_Dirs`,`IPScanner`.`Team`,`IPScanner`.`Manufacturer`, "
							+ "`IPScanner`.`MAC_Address` "
							+ "FROM `mydb`.`IPScanner` ");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}		
		
		System.out.println(results.wasNull());
		try {
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {  
		        
	            Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);

			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}

		return model;
		
	}
	
}


























