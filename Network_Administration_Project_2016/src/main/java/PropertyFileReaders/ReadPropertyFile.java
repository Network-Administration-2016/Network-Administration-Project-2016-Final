package PropertyFileReaders;

import java.io.*;

import java.util.Properties;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class ReadPropertyFile {
	
	private Properties prop = null;
	private FileReader reader ;
	
	/**
	 * 
	 * @param path: relative path to open
	 */
	public ReadPropertyFile(String path){
		File properties_file = new File(path);
		this.prop = new Properties();
		
		try {
			this.reader = new FileReader(properties_file) ;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
				
		try {
			prop.load(reader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 
	 * @return the character font for the gui
	 */
	public String getCharacterFont(){
		String character_font = prop.getProperty("Character_Font");
		return character_font;
	}
	
	/**
	 * 
	 * @return the font number for the gui
	 */
	public int getFontNumber(){
		String font_number = prop.getProperty("Font_Number");
		return Integer.parseInt(font_number);
	}
	
	/**
	 * 
	 * @return font for messages
	 */
	public String getCharacterFontMessages(){
		String character_font = prop.getProperty("Character_Font_Messages");
		return character_font;
	}
	
	/**
	 * 
	 * @return font number for messages
	 */
	public int getFontNumberMessages(){
		String font_number = prop.getProperty("Font_Number_Messages");
		return Integer.parseInt(font_number);
	}

}

